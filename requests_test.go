package gitlab

import (
	"bytes"
	"gitlab.com/dualwield/gitlab/api"
	"gitlab.com/dualwield/gitlab/model"
	"io"
	"net/http"
	"reflect"
	"testing"
)

func TestGroups(t *testing.T) {
	type args struct {
		sess *api.RestSession
		kvxs []api.KVer
	}
	tests := []struct {
		name    string
		args    args
		wantGxs []model.Group
		wantErr bool
	}{
		{
			name: "list groups happy",
			args: args{
				sess: api.New("https://test.dev", "xyz123", NewTestHTTP(nil, sampleRS())),
				kvxs: nil,
			},
			wantGxs: []model.Group{
				{ID: 7, Name: "TEST7", Path: "TEST7lib"},
			},
			wantErr: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			gotGxs, err := Groups(tt.args.sess, tt.args.kvxs...)
			if (err != nil) != tt.wantErr {
				t.Errorf("List() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(gotGxs, tt.wantGxs) {
				t.Errorf("List() gotGxs = %v, want %v", gotGxs, tt.wantGxs)
			}
		})
	}
}

func NewTestHTTP(err error, rs *http.Response) TestHTTP {
	return TestHTTP{
		Err: err,
		RS:  rs,
	}
}

type TestHTTP struct {
	Err error
	RS  *http.Response
}

func (t TestHTTP) Do(*http.Request) (*http.Response, error) {
	return t.RS, t.Err
}

func sampleRS() *http.Response {
	reader := bytes.NewReader([]byte(`[{"id":7,"name":"TEST7","path":"TEST7lib"}]`))

	return &http.Response{
		StatusCode: 200,
		Body:       io.NopCloser(reader),
	}
}
