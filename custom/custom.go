package custom

import (
	"gitlab.com/dualwield/gitlab/api"
)

func Call(sess *api.RestSession, resource, method string, watcher api.RSWatcher, kvxs ...api.KVer) error {
	call := api.NewCall(sess, method, resource)
	call = call.With(kvxs...)

	return call.Send(watcher)
}
