# Dualwield Gitlab Lib

### TODO
I have been working on too many projects, and when that happens, unfinished
projects tend to get lost in the shuffle. I am going to document
some of the Gitlab API I would like to cover in the near future.

- [x] Groups API
- [ ] Projects API
- [ ] Pipeline API
- [ ] Badges (projects and groups) API
- [ ] Jobs API

For now.. this is it. A new checklist will come once this gets achieved. 