package api

import (
	"bytes"
	"net/http"
	"strings"
)

func New(url string, token string, client HTTPDoer) *RestSession {
	if client == nil {
		client = http.DefaultClient
	}
	return &RestSession{
		client:  client,
		host:    url,
		token:   token,
		version: "/api/v4/",
	}
}

type RestSession struct {
	client  HTTPDoer
	host    string
	token   string
	version string
}

func (s *RestSession) CallGitlab(method, url string, body []byte) (*http.Response, error) {
	rq, err := http.NewRequest(method,
		strings.TrimRight(s.host, "/")+"/"+strings.Trim(s.version, "/")+"/"+strings.TrimLeft(url, "/"),
		bytes.NewBuffer(body))
	if err != nil {
		return nil, err
	}
	rq.Header.Add("PRIVATE-TOKEN", s.token)

	return s.client.Do(rq)
}

type HTTPDoer interface {
	Do(*http.Request) (*http.Response, error)
}
