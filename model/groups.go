package model

import (
	"time"
)

type Group struct {
	ID                             int64       `json:"id,omitempty"`
	Name                           string      `json:"name,omitempty"`
	Path                           string      `json:"path,omitempty"`
	Description                    string      `json:"description,omitempty"`
	Visibility                     string      `json:"visibility,omitempty"`
	ShareWithGroupLock             bool        `json:"share_with_group_lock,omitempty"`
	RequireTwoFactorAuthentication bool        `json:"require_two_factor_authentication,omitempty"`
	TwoFactorGracePeriod           int64       `json:"two_factor_grace_period,omitempty"`
	ProjectCreationLevel           string      `json:"project_creation_level,omitempty"`
	AutoDevopsEnabled              interface{} `json:"auto_devops_enabled,omitempty"`
	SubgroupCreationLevel          string      `json:"subgroup_creation_level,omitempty"`
	EmailsDisabled                 interface{} `json:"emails_disabled,omitempty"`
	MentionsDisabled               interface{} `json:"mentions_disabled,omitempty"`
	LfsEnabled                     bool        `json:"lfs_enabled,omitempty"`
	DefaultBranchProtection        int64       `json:"default_branch_protection,omitempty"`
	AvatarURL                      string      `json:"avatar_url,omitempty"`
	WebURL                         string      `json:"web_url,omitempty"`
	RequestAccessEnabled           bool        `json:"request_access_enabled,omitempty"`
	FullName                       string      `json:"full_name,omitempty"`
	FullPath                       string      `json:"full_path,omitempty"`
	FileTemplateProjectID          int64       `json:"file_template_project_id,omitempty"`
	ParentID                       interface{} `json:"parent_id,omitempty"`
	CreatedAt                      time.Time   `json:"created_at,omitempty"`
	Statistics                     *Statistics `json:"statistics,omitempty"`
}

type CreateGroupRS struct {
	ID                             int64         `json:"id,omitempty"`
	WebURL                         string        `json:"web_url,omitempty"`
	Name                           string        `json:"name,omitempty"`
	Path                           string        `json:"path,omitempty"`
	Description                    string        `json:"description,omitempty"`
	Visibility                     string        `json:"visibility,omitempty"`
	ShareWithGroupLock             bool          `json:"share_with_group_lock,omitempty"`
	RequireTwoFactorAuthentication bool          `json:"require_two_factor_authentication,omitempty"`
	TwoFactorGracePeriod           int64         `json:"two_factor_grace_period,omitempty"`
	ProjectCreationLevel           string        `json:"project_creation_level,omitempty"`
	AutoDevopsEnabled              interface{}   `json:"auto_devops_enabled"`
	SubgroupCreationLevel          string        `json:"subgroup_creation_level,omitempty"`
	EmailsDisabled                 interface{}   `json:"emails_disabled"`
	MentionsDisabled               interface{}   `json:"mentions_disabled"`
	LfsEnabled                     bool          `json:"lfs_enabled,omitempty"`
	DefaultBranchProtection        int64         `json:"default_branch_protection,omitempty"`
	AvatarURL                      interface{}   `json:"avatar_url"`
	RequestAccessEnabled           bool          `json:"request_access_enabled,omitempty"`
	FullName                       string        `json:"full_name,omitempty"`
	FullPath                       string        `json:"full_path,omitempty"`
	CreatedAt                      time.Time     `json:"created_at,omitempty"`
	ParentID                       int64         `json:"parent_id,omitempty"`
	LdapCn                         interface{}   `json:"ldap_cn"`
	LdapAccess                     interface{}   `json:"ldap_access"`
	SharedWithGroups               []interface{} `json:"shared_with_groups"`
	Projects                       []interface{} `json:"projects"`
	SharedProjects                 []interface{} `json:"shared_projects"`
	SharedRunnersMinutesLimit      interface{}   `json:"shared_runners_minutes_limit"`
	ExtraSharedRunnersMinutesLimit interface{}   `json:"extra_shared_runners_minutes_limit"`
	PreventForkingOutsideGroup     interface{}   `json:"prevent_forking_outside_group"`
}

type GroupDetail struct {
	MarkedForDeletion                    *time.Time    `json:"marked_for_deletion,omitempty"`
	ID                                   int64         `json:"id,omitempty"`
	WebURL                               string        `json:"web_url,omitempty"`
	Name                                 string        `json:"name,omitempty"`
	Path                                 string        `json:"path,omitempty"`
	Description                          string        `json:"description,omitempty"`
	Visibility                           string        `json:"visibility,omitempty"`
	ShareWithGroupLock                   bool          `json:"share_with_group_lock,omitempty"`
	RequireTwoFactorAuthentication       bool          `json:"require_two_factor_authentication,omitempty"`
	TwoFactorGracePeriod                 int64         `json:"two_factor_grace_period,omitempty"`
	ProjectCreationLevel                 string        `json:"project_creation_level,omitempty"`
	AutoDevopsEnabled                    bool          `json:"auto_devops_enabled"`
	SubgroupCreationLevel                string        `json:"subgroup_creation_level,omitempty"`
	EmailsDisabled                       bool          `json:"emails_disabled"`
	MentionsDisabled                     bool          `json:"mentions_disabled"`
	LfsEnabled                           bool          `json:"lfs_enabled,omitempty"`
	DefaultBranchProtection              int64         `json:"default_branch_protection,omitempty"`
	AvatarURL                            string        `json:"avatar_url,omitempty"`
	RequestAccessEnabled                 bool          `json:"request_access_enabled,omitempty"`
	FullName                             string        `json:"full_name,omitempty"`
	FullPath                             string        `json:"full_path,omitempty"`
	CreatedAt                            time.Time     `json:"created_at,omitempty"`
	ParentID                             int64         `json:"parent_id,omitempty"`
	LdapCn                               interface{}   `json:"ldap_cn"`     //TODO
	LdapAccess                           interface{}   `json:"ldap_access"` //TODO
	SharedWithGroups                     []GroupShared `json:"shared_with_groups,omitempty"`
	RunnersToken                         string        `json:"runners_token,omitempty"`
	PreventSharingGroupsOutsideHierarchy bool          `json:"prevent_sharing_groups_outside_hierarchy,omitempty"`
	SharedProjects                       []interface{} `json:"shared_projects"`                    //TODO
	SharedRunnersMinutesLimit            interface{}   `json:"shared_runners_minutes_limit"`       //TODO
	ExtraSharedRunnersMinutesLimit       interface{}   `json:"extra_shared_runners_minutes_limit"` //TODO
	PreventForkingOutsideGroup           interface{}   `json:"prevent_forking_outside_group"`      //TODO
}

type GroupShared struct {
	ID          int64      `json:"group_id,omitempty"`
	Name        string     `json:"group_name,omitempty"`
	FullPath    string     `json:"group_full_path,omitempty"`
	AccessLevel string     `json:"group_access_level,omitempty"`
	Expires     *time.Time `json:"expires_at,omitempty"`
}
