package api

import (
	"net/http"
)

type RSWatcher func(*RS) (stop bool)

type RS struct {
	Err      error
	Response *http.Response
	NextPage *Call
	Payload  []byte
}
