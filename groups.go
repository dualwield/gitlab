package gitlab

import (
	"encoding/json"
	"fmt"
	"gitlab.com/dualwield/gitlab/api"
	"gitlab.com/dualwield/gitlab/model"
	"net/http"
)

// All the requests made from the Gitlab Groups resource are found in this file.
// This is my attempt at keeping the code organized since as of now its starting
// to get messy, and I can foresee where this is going. Cross your fingers. - Jab

// Groups covers the GET /groups endpoint
func Groups(sess *api.RestSession, kvxs ...api.KVer) (gxs []model.Group, err error) {
	call := api.NewCall(sess, http.MethodGet, "/groups")
	call = call.With(kvxs...)

	var watcher WatchGroups
	if err = call.Send(watcher.Watch); err != nil {
		return
	}
	gxs, err = watcher.Groups, watcher.Err

	return
}

// Subgroups covers the GET /groups/:id/subgroups endpoint
func Subgroups(sess *api.RestSession, groupID int64, kvxs ...api.KVer) (gxs []model.Group, err error) {
	call := api.NewCall(sess, http.MethodGet, fmt.Sprintf("/groups/%d/subgroups", groupID))
	call = call.With(kvxs...)

	var watcher WatchGroups
	if err = call.Send(watcher.Watch); err != nil {
		return
	}
	gxs, err = watcher.Groups, watcher.Err

	return
}

// GroupDescendants covers the GET /groups/:id/descendant_groups endpoint
func GroupDescendants(sess *api.RestSession, groupID int64, kvxs ...api.KVer) (gxs []model.Group, err error) {
	call := api.NewCall(sess, http.MethodGet, fmt.Sprintf("/groups/%d/descendant_groups", groupID))
	call = call.With(kvxs...)

	var watcher WatchGroups
	if err = call.Send(watcher.Watch); err != nil {
		return
	}
	gxs, err = watcher.Groups, watcher.Err

	return watcher.Groups, nil
}

// GroupProjects covers the GET /groups/:id/projects endpoint
func GroupProjects(sess *api.RestSession, groupID int64, kvxs ...api.KVer) (pxs []model.Project, err error) {
	call := api.NewCall(sess, http.MethodGet, fmt.Sprintf("/groups/%d/projects", groupID))
	call = call.With(kvxs...)

	var watcher WatchGroupProjects
	if err = call.Send(watcher.Watch); err != nil {
		return
	}
	pxs, err = watcher.Projects, watcher.Err

	return
}

// GroupSharedProjects covers the GET /groups/:id/projects/shared endpoint
func GroupSharedProjects(sess *api.RestSession, groupID int64, kvxs ...api.KVer) (pxs []model.Project, err error) {
	call := api.NewCall(sess, http.MethodGet, fmt.Sprintf("/groups/%d/projects/shared", groupID))
	call = call.With(kvxs...)

	var watcher WatchGroupProjects
	if err = call.Send(watcher.Watch); err != nil {
		return
	}
	pxs, err = watcher.Projects, watcher.Err
	return
}

// GroupDetails covers the /groups/:id endpoint
func GroupDetails(sess *api.RestSession, groupID int64, kvs ...api.KVer) (gd model.GroupDetail, err error) {
	call := api.NewCall(sess, http.MethodGet, fmt.Sprintf("/groups/%d", groupID))
	call = call.With(kvs...)

	var watcher WatchGroupDetail
	if err = call.Send(watcher.Watch); err != nil {
		return
	}
	gd, err = watcher.GroupDetail, watcher.Err
	return
}

// GroupCreate covers the POST /groups endpoint (and creates a non top level group)
func GroupCreate(sess *api.RestSession, parentID int64, name, path string, kvs ...api.KVer) (g model.Group, err error) {
	call := api.NewCall(sess, http.MethodPost, "/groups")
	call = call.With(
		api.Parameter{"parent_id", parentID},
		api.Parameter{"name", name},
		api.Parameter{"path", path},
	)
	call = call.With(kvs...)

	var watchOne WatchGroup
	if err = call.Send(watchOne.Watch); err != nil {
		return
	}
	g, err = watchOne.Group, watchOne.Err

	return
}

// GroupUpdate covers the PUT /groups/:id endpoint
func GroupUpdate(sess *api.RestSession, id int64, kvxs ...api.KVer) (g model.Group, err error) {
	call := api.NewCall(sess, http.MethodPut, fmt.Sprintf("/groups/%d", id))
	call = call.With(kvxs...)

	var watchOne WatchGroup
	if err = call.Send(watchOne.Watch); err != nil {
		return
	}
	g, err = watchOne.Group, watchOne.Err
	return
}

// GroupRemove covers the DELETE /groups/:id endpoint
func GroupRemove(sess *api.RestSession, id int64) (removed bool, err error) {
	call := api.NewCall(sess, http.MethodDelete, fmt.Sprintf("/groups/%d", id))

	watch := func(_rs *api.RS) (stop bool) {
		stop = true
		removed = _rs.Response.StatusCode == http.StatusAccepted
		err = _rs.Err
		return
	}
	if err := call.Send(watch); err != nil {
		return false, err
	}
	return
}

//
// Watchers
//

type WatchGroup struct {
	Err   error
	Group model.Group
}

// Watch checks the RS payload for a single Group, collects it and stops pagination
func (w *WatchGroup) Watch(_rs *api.RS) (stop bool) {
	stop = true // On a single item response, stopping will only save the Caller from checking pagination

	if _rs.Payload != nil {
		w.Err = json.Unmarshal(_rs.Payload, &w.Group)
	}
	return
}

type WatchGroups struct {
	Err    error
	Groups []model.Group
}

func (w *WatchGroups) Watch(_rs *api.RS) (stop bool) {
	if _rs.Payload != nil {
		var gxs []model.Group
		if err := json.Unmarshal(_rs.Payload, &gxs); err != nil {
			w.Err = err
			stop = true
			return
		}
		w.Groups = append(w.Groups, gxs...)
	}
	stop = true
	return
}

type WatchCreateGroups struct {
	Group model.Group
	Err   error
}

func (g *WatchCreateGroups) Watch(_rs *api.RS) (stop bool) {
	// Only one group should return with this call, so stop is already set to true
	stop = true

	if err := json.Unmarshal(_rs.Payload, &g.Group); err != nil {
		g.Err = err
	}
	return
}

type WatchGroupProjects struct {
	Err      error
	Projects []model.Project
}

func (c *WatchGroupProjects) Watch(_rs *api.RS) (stop bool) {
	if _rs.Payload != nil {
		var pxs []model.Project
		if err := json.Unmarshal(_rs.Payload, &pxs); err != nil {
			c.Err = err
			stop = true
			return
		}
		c.Projects = append(c.Projects, pxs...)
	}
	return
}

type WatchGroupDetail struct {
	Err         error
	GroupDetail model.GroupDetail
}

func (w *WatchGroupDetail) Watch(_rs *api.RS) (stop bool) {
	stop = true

	if _rs.Payload != nil {
		w.Err = json.Unmarshal(_rs.Payload, &w.GroupDetail)
	}
	return
}
