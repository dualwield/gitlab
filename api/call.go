package api

import (
	"fmt"
	"github.com/pkg/errors"
	"io/ioutil"
	"strconv"
	"strings"
)

type KVer interface {
	KV() string
}

type Parameter [2]interface{}

func (p Parameter) KV() string {
	return fmt.Sprintf("%v=%v", p[0], p[1])
}

type Filter [2]string

func (f Filter) KV() string {
	return fmt.Sprintf("custom_attributes[%s]=%s", f[0], f[1])
}

type WithStatistics bool

func (f WithStatistics) KV() string {
	if f {
		return "statistics=true"
	}
	return ""
}

type ResourceConfig func(*Call)

func NewCall(sess *RestSession, method, resource string) Call {
	return Call{
		ResourceEndpoint: resource,
		Parameters:       map[string]bool{},
		PathParameters:   map[string]interface{}{},
		Session:          sess,
		page:             1,
		perPage:          20,
		method:           method,
	}
}

type Call struct {
	body             []byte
	method           string
	page             int
	perPage          int
	Parameters       map[string]bool
	PathParameters   map[string]interface{}
	ResourceEndpoint string
	Session          *RestSession
}

func (c Call) With(xs ...KVer) Call {
	for _, kv := range xs {
		c.Parameters[kv.KV()] = true
	}
	return c
}

func (c Call) Path(key string, value interface{}) Call {
	c.PathParameters[key] = value
	return c
}

func (c Call) Send(watcher RSWatcher) error {
	for k, v := range c.PathParameters {
		vv := fmt.Sprintf("%v", v)
		c.ResourceEndpoint = strings.ReplaceAll(c.ResourceEndpoint, k, vv)
	}
	c = c.With(Parameter{"per_page", c.perPage}, Parameter{"page", c.page})

	// add page and per page to pathParameters
	var params []string
	for k, _ := range c.Parameters {
		params = append(params, k)
	}
	paramURL := strings.Join(params, "&")
	var url string
	if len(paramURL) > 0 {
		url = c.ResourceEndpoint + "?" + paramURL
	}

	var rs RS
	httpRS, err := c.Session.CallGitlab(c.method, url, c.body)
	if err != nil {
		rs.Err = err //TODO consider whether rs needs the error field...
		return err
	}
	rs.Response = httpRS

	if httpRS.Body != nil {
		rs.Payload, rs.Err = ioutil.ReadAll(httpRS.Body)
		defer httpRS.Body.Close()
	}

	// This is when the passed in RS Watcher can see the data coming in, and decide to
	// stop or to continue. Unless the Watcher decides to stop, if another Call is in queue
	// [Pagination], then this Caller will continue paginating until complete.
	//
	// If the Logic does not want to paginate through calls, then the RSWatcher needs to monitor the
	// page attributes of the Gitlab Response
	if watcher != nil {
		stop := watcher(&rs)
		if stop {
			return nil
		}
	}

	var next bool
	if nextPage := rs.Response.Header.Get("x-next-page"); nextPage != "" {
		if page, err := strconv.Atoi(nextPage); err != nil {
			rs.Err = errors.Wrap(err, "failure converting nextpage to int")
		} else {
			c.page = page
			next = true
		}
	}
	if next {
		return c.Send(watcher)
	}
	return nil
}
