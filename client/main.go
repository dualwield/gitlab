package main

import (
	"encoding/json"
	"fmt"
	"gitlab.com/dualwield/gitlab"
	"gitlab.com/dualwield/gitlab/api"
	"gitlab.com/dualwield/golib/zodiac"
)

func main() {
	zodiac.Load()
	sess := api.New("https://gitlab.com", zodiac.Lookup("GITLAB_TOKEN").Val(), nil)

	project, err := gitlab.Project(sess, 34148025, api.Parameter{"statistics", true})
	if err != nil {
		fmt.Println(err)
	}

	b, err := json.MarshalIndent(project, " ", "  ")
	if err != nil {
		fmt.Println(err)
		return
	}
	fmt.Println(string(b))
}
