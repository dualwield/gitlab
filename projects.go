package gitlab

import (
	"encoding/json"
	"fmt"
	"gitlab.com/dualwield/gitlab/api"
	"gitlab.com/dualwield/gitlab/model"
	"net/http"
)

// Project (trying to think of a better name still) covers the GET /projects/:id endpoint
func Project(sess *api.RestSession, projectID int64, kvxs ...api.KVer) (p model.Project, err error) {
	call := api.NewCall(sess, http.MethodGet, fmt.Sprintf("/projects/%d", projectID))
	call = call.With(kvxs...)

	var watcher WatchProject
	if err = call.Send(watcher.Watch); err != nil {
		return
	}
	p, err = watcher.Project, watcher.Err
	return
}

// Create covers the POST /projects endpoint
func Create(sess *api.RestSession, namespaceID int64, name, path string, kvxs ...api.KVer) (p model.Project, err error) {
	call := api.NewCall(sess, http.MethodPost, "/projects")
	call = call.With(
		api.Parameter{"namespace_id", namespaceID},
		api.Parameter{"name", name},
		api.Parameter{"path", path})
	call = call.With(kvxs...)

	watcher := WatchProject{}
	err = call.Send(watcher.Watch)
	if err != nil {
		return
	}
	p, err = watcher.Project, watcher.Err
	return
}

// Edit covers the PUT /projects/:id endpoint
func Edit(sess *api.RestSession, id int64, kvxs ...api.KVer) (p model.Project, err error) {
	call := api.NewCall(sess, http.MethodPut, fmt.Sprintf("/projects/%d", id))
	call = call.With(kvxs...)

	var watcher WatchProject
	if err = call.Send(watcher.Watch); err != nil {
		return
	}
	p, err = watcher.Project, watcher.Err
	return
}

//
// Watchers
//

type WatchProject struct {
	Err     error
	Project model.Project
}

func (w *WatchProject) Watch(_rs *api.RS) (stop bool) {
	stop = true

	if _rs.Payload != nil {
		w.Err = json.Unmarshal(_rs.Payload, &w.Project)
	}
	return
}
