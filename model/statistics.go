package model

type Statistics struct {
	StorageSize           int `json:"storage_size"`
	RepositorySize        int `json:"repository_size"`
	WikiSize              int `json:"wiki_size"`
	LfsObjectsSize        int `json:"lfs_objects_size"`
	JobArtifactsSize      int `json:"job_artifacts_size"`
	PipelineArtifactsSize int `json:"pipeline_artifacts_size"`
	PackagesSize          int `json:"packages_size"`
	SnippetsSize          int `json:"snippets_size"`
	UploadsSize           int `json:"uploads_size"`
}
